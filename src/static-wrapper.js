import statics from '@/assets/data/static-text.json';

export default ({
  getImprint() {
    return statics[Object.keys(statics).find(element => statics[element].key === 'impressum')];
  },

  getHome() {
    return statics[Object.keys(statics).find(element => statics[element].key === 'intro')];
  },
});
