import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Tours from './views/Tours.vue';
import Exhibits from './views/Exhibits.vue';
import Events from './views/Events.vue';
import About from './views/About.vue';
import Imprint from './views/Imprint.vue';
import ExhibitDetail from './views/ExhibitDetail.vue';
import Timeline from './views/Timeline.vue';
import EventDetail from './views/EventDetail.vue';
import Game from './views/Game.vue';

Vue.use(Router);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: { title: 'Startseite' },
  },
  {
    path: '/exhibits/:id',
    component: ExhibitDetail,
    meta: { navigateBack: true },
  },
  {
    path: '/exhibits',
    name: 'exhibits',
    component: Exhibits,
    meta: { title: 'Ausstellungsobjekte' },
  },
  {
    path: '/game',
    name: 'game',
    component: Game,
    meta: { title: 'Quiz' },
  },
  {
    path: '/timeline',
    name: 'timeline',
    component: Timeline,
    meta: { title: 'Zeitstrahl' },
  },
  {
    path: '/tours',
    name: 'tours',
    component: Tours,
    meta: { title: 'Touren' },
  },
  {
    path: '/events/:location/:id',
    component: EventDetail,
    meta: { navigateBack: true },
  },
  {
    path: '/events',
    name: 'events',
    component: Events,
    meta: { title: 'Veranstaltungen' },
  },
  {
    path: '/about',
    name: 'about',
    component: About,
    meta: { title: 'Über das Museum' },
  },
  {
    path: '/imprint',
    name: 'imprint',
    component: Imprint,
    meta: { title: 'Impressum' },
  },
];

export default new Router({
  routes,
  linkActiveClass: '',
  linkExactActiveClass: 'active',
});
