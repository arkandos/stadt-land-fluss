import exhibits from '@/assets/data/exhibits.json';
import tours from '@/assets/data/tours.json';

export default ({
  getData() {
    return Object.keys(exhibits).map(key => exhibits[key]);
  },

  getExhibit(id) {
    return exhibits[Object.keys(exhibits).find(element => element === id)];
  },

  sortExhibits(exhibit1, exhibit2) {
    const regex = /\b\d{3,4}\b|\d{2}\./;
    if (!exhibit1.dateinfo) {
      return 1;
    } else if (!exhibit2.dateinfo) {
      return -1;
    }
    let exhDate1 = parseInt(exhibit1.dateinfo.match(regex), 10);
    let exhDate2 = parseInt(exhibit2.dateinfo.match(regex), 10);
    if (Number.isNaN(exhDate1)) {
      return 1;
    } else if (Number.isNaN(exhDate2)) {
      return -1;
    } else if (exhDate1 < 21) {
      exhDate1 *= 100;
    } else if (exhDate2 < 21) {
      exhDate2 *= 100;
    }
    return exhDate1 - exhDate2;
  },

  getTours() {
    return Object.keys(tours).map(key => tours[key]);
  },
});
