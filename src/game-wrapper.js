import questions from '@/assets/data/questions.json';

export default ({
  getData() {
    return Object.keys(questions).map(key => questions[key]);
  },
});
