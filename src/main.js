/* eslint-disable no-console */
import Vue from 'vue';
import VueImg from 'v-img';
import App from './App.vue';
import router from './router';
import './css/main.css';

Vue.use(VueImg);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
