const https = require('https');
const fs = require('fs');
const path = require('path');

function download(url, dest, cb) {
  const file = fs.createWriteStream(dest, { flags: 'w' });
  https.get(url, (response) => {
    response.pipe(file);
    file.on('finish', () => {
      file.close(cb); // close() is async, call cb after close completes.
    });
  }).on('error', (err) => { // Handle errors
    fs.unlink(dest); // Delete the file async. (But we don't check the result)
    if (cb) cb(err.message);
  });
}

let exportUrl = 'https://apps.regensburg.de/sixcms/detail.php?template=jsonexponate';
let filename = 'exhibits.json';

let downloadPath = path.join(__dirname, '../src/assets/data/', filename);

download(exportUrl, downloadPath, (err) => {
  if (err) {
    console.log('Error while downloading file: ', err);
    process.exit(127);
  } else {
    console.log('Download finished.');
  }
});

exportUrl = 'https://apps.regensburg.de/sixcms/detail.php?template=jsontouren';
filename = 'tours.json';

downloadPath = path.join(__dirname, '../src/assets/data/', filename);

download(exportUrl, downloadPath, (err) => {
  if (err) {
    console.log('Error while downloading file: ', err);
    process.exit(127);
  } else {
    console.log('Download finished.');
  }
});

exportUrl = 'https://apps.regensburg.de/sixcms/detail.php?template=jsontexte';
filename = 'static-text.json';

downloadPath = path.join(__dirname, '../src/assets/data/', filename);

download(exportUrl, downloadPath, (err) => {
  if (err) {
    console.log('Error while downloading file: ', err);
    process.exit(127);
  } else {
    console.log('Download finished.');
  }
});
