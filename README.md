# Stadt-Land-Fluss

Eine PWA (Progressive Web App) zum diesjährigen Motto des kulturellen Programms der Stadt Regensburg. Die App soll die Gäste vor, während und nach ihrem Besuch durch hilfreiche Informationen, Guides und Gewinnspielen begleiten.

# Entwicklung

Folgende Tools _müssen_ installiert sein:

* [Node.JS](https://nodejs.org/en/)
* [GIT](https://git-scm.com/)
    * [cmder](http://cmder.net/) - Ein alternativer Konsolenemulator (Das "`cmd`" - Fenster) für Windows mit eingebauter Git-Unterstütung
    * [Git for Windows](https://git-scm.com/downloads) - Git-Distribution mit "echter" Bash
    * [Github Desktop](https://desktop.github.com/) - Github-spezifische grafische Benutzeroberfläche

Empfohlenes Editor-Setup:

* [Visual Studio Code](https://code.visualstudio.com/) - Javascript Editor
    * [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur) - Vue tooling für VS Code: Syntax-Highlighting von `.vue` - Dateien, Scaffolding, etc.
    * [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - ESLint-Integration in VS Code.
* [Vue Devtools](https://github.com/vuejs/vue-devtools) ([Chrome](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd) / [Firefox](https://addons.mozilla.org/en-US/firefox/addon/vue-js-devtools/)) - Browser-Plugin, das Vue-Komponenten und deren internen Zustand in den Entwicklertools anzeigt.


## Entwicklicker-Abhängigkeiten

Alle weiteren Abhängigkeiten, die zum Entwickeln und Kompilieren der App benötigt werden, werden über den Node Package Manager installiert:

```bash
$ npm install
```

Folgende Tools und Bibliotheken werden verwendet:

* [Vue.js](https://vuejs.org/) - JavaScript Framework für interaktive Benutzeroberflächen
    * [Learn Vue 2: Step by Step](https://laracasts.com/series/learn-vue-2-step-by-step)
    * [The Vue Handbook: a thorough introduction to Vue.js](https://medium.freecodecamp.org/the-vue-handbook-a-thorough-introduction-to-vue-js-1e86835d8446)
* [vue-router](https://router.vuejs.org/) - Client-seitige Seitennavigation
* [Babel](https://babeljs.io/) - Javascript Compiler, der fortgeschrittene und experimentelle Sprachfunktionen in einfachere Repräsentationen umformt, um die Browserkompatibilität zu erhöhen
    * [Learn ES2015](https://babeljs.io/docs/en/learn/) - Stellt neue Features in ES6 vor, die von Babel unterstützt werden
* [ESLint](https://eslint.org/) - Zusätzliches statisches Analysetool für Javascript-Style, Best-Practicces, etc.
    * [Airbnb Style Guide](https://github.com/airbnb/javascript) - Verwendetes Regelset
* [PostCSS](https://postcss.org/) - _"Babel für CSS"_ - Fügt u.a. Browser-spezifische Regeln ein für Versionen, die den offiziellen Standard noch nicht unterstützen (siehe auch [autoprefixer](https://github.com/postcss/autoprefixer))

## Testen und Kompilieren

Nach der Installation der Abhängigkeiten kann ein Entwicklungsserver mithilfe von

```bash
$ npm start
```

gestartet werden. Dieser aktualisiert automatisch den Browser, sobald Änderungen an Dateien vorgenommen werden, sodass die Seite nicht nach jedem Speichern neu geladen werden muss.

Eine Version, die zur Auslieferung optimiert wurde, kann mithilfe von

```bash
$ npm run build
```

erstellt werden.

## Aufbau des Projekts

| Ordner | Beschreibung |
|--------|--------------|
| `dist/` | Auslieferbare, kompilierte Version aller Dateien, erstellt mit `npm run build` |
| `public/` | Zusätzliche statische Assets, die nicht in Vue-Komponenten verwendet werden, wie Logos/Favicons |
| `src/` | Ordner für alle Quelldateien, aus denen die App besteht. |
| `src/assets/` | Bilder und andere Designelemente |
|  `src/css/` | CSS-Stylesheets |
| `src/fonts/` | Verwendete Schriftarten |
| `src/views/` | Vue-Komponenten, die als eigene Seite gerendert werden |
| `src/components/` | Andere Vue-Komponenten, die in Seiten verwendet werden |

