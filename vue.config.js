// vue.config.js

module.exports = {
  pwa: {
    name: 'slf',
    themeColor: '#E30613',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
    workboxPluginMode: 'GenerateSW',
    workboxOptions: {
      importWorkboxFrom: 'local',
    },
  },
};
